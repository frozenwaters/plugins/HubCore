package fun.lewisdev.deluxehub.module;

public enum ModuleType {

    ANTI_WDL,
    CUSTOM_COMMANDS,
    DOUBLE_JUMP,
    LAUNCHPAD,
    SCOREBOARD,
    ANNOUNCEMENTS,
    WORLD_PROTECT,
    ANTI_SWEAR,
    COMMAND_BLOCK,
    LOBBY,
    HOTBAR_ITEMS,
    PLAYER_LISTENER,
    PLAYER_OFFHAND_LISTENER

}
