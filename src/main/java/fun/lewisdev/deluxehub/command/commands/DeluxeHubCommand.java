package fun.lewisdev.deluxehub.command.commands;

import cl.bgmp.minecraft.util.commands.CommandContext;
import cl.bgmp.minecraft.util.commands.annotations.Command;
import cl.bgmp.minecraft.util.commands.exceptions.CommandException;
import fun.lewisdev.deluxehub.DeluxeHubPlugin;
import fun.lewisdev.deluxehub.Permissions;
import fun.lewisdev.deluxehub.command.CommandManager;
import fun.lewisdev.deluxehub.config.Messages;
import fun.lewisdev.deluxehub.inventory.AbstractInventory;
import fun.lewisdev.deluxehub.inventory.InventoryManager;
import fun.lewisdev.deluxehub.module.ModuleManager;
import fun.lewisdev.deluxehub.module.ModuleType;
import fun.lewisdev.deluxehub.module.modules.hotbar.HotbarItem;
import fun.lewisdev.deluxehub.module.modules.hotbar.HotbarManager;
import fun.lewisdev.deluxehub.module.modules.visual.scoreboard.ScoreboardManager;
import fun.lewisdev.deluxehub.module.modules.world.LobbySpawn;
import fun.lewisdev.deluxehub.utility.TextUtil;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import java.util.stream.Collectors;

public class DeluxeHubCommand {

    private DeluxeHubPlugin plugin;

    public DeluxeHubCommand(DeluxeHubPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = {"hubcore", "hcore", "hub"},
            desc = "View plugin information"
    )
    public void main(final CommandContext args, final CommandSender sender) throws CommandException {

        PluginDescriptionFile pdfFile = plugin.getDescription();

		/*
		Command: help
		Description: displays help message
		*/
        if (args.argsLength() == 0 || args.getString(0).equalsIgnoreCase("help")) {

            if (!sender.hasPermission(Permissions.COMMAND_DELUXEHUB_HELP.getPermission())) {
                sender.sendMessage(TextUtil.color("&3&lFrozen&b&lWaters &b- &7Running HubCore &b" + plugin.getDescription().getVersion() + "&7."));
                return;
            }

            sender.sendMessage("");
            sender.sendMessage(TextUtil.color("&3&lFrozen&b&lWaters &b - Hub Core " + "&e(v" + plugin.getDescription().getVersion() + ")"));
            sender.sendMessage("");
            sender.sendMessage(TextUtil.color(" &d/hubcore info &8- &7&oDisplays information about the current config"));
            sender.sendMessage(TextUtil.color(" &d/hubcore scoreboard &8- &7&oToggle the scoreboard"));
            sender.sendMessage(TextUtil.color(" &d/hubcore open <menu> &8- &7&oOpen a custom menu"));
            sender.sendMessage("");
            sender.sendMessage(TextUtil.color("  &d/fly &8- &7&oToggle flight mode"));
            sender.sendMessage(TextUtil.color("  &d/setspawn &8- &7&oSet the spawn location"));
            sender.sendMessage(TextUtil.color("  &d/spawn &8- &7&oTeleport to the spawn location"));
            sender.sendMessage(TextUtil.color("  &d/gamemode <gamemode> &8- &7&oSet your gamemode"));
            sender.sendMessage(TextUtil.color("  &d/gmc &8- &7&oGo into creative mode"));
            sender.sendMessage(TextUtil.color("  &d/gms &8- &7&oGo into survival mode"));
            sender.sendMessage(TextUtil.color("  &d/gma &8- &7&oGo into adventure mode"));
            sender.sendMessage(TextUtil.color("  &d/gmsp &8- &7&oGo into spectator mode"));
            sender.sendMessage("");
            return;
        }

		/*
		Command: reload
		Description: reloads the entire plugin
		*/
        else if (args.getString(0).equalsIgnoreCase("reload")) {

            if (!sender.hasPermission(Permissions.COMMAND_DELUXEHUB_RELOAD.getPermission())) {
                sender.sendMessage(Messages.NO_PERMISSION.toString());
                return;
            }

            long start = System.currentTimeMillis();
            plugin.reload();
            sender.sendMessage(Messages.CONFIG_RELOAD.toString().replace("%time%", String.valueOf(System.currentTimeMillis() - start)));
            int inventories = plugin.getInventoryManager().getInventories().size();
            if (inventories > 0) {
                sender.sendMessage(TextUtil.color("&8- &7Loaded &a" + inventories + "&7 custom menus."));
            }
        }

		/*
		Command: scoreboard
		Description: toggles the scoreboard on/off
		*/
        else if (args.getString(0).equalsIgnoreCase("scoreboard")) {

            if (!(sender instanceof Player)) throw new CommandException("Console cannot toggle the scoreboard");

            if (!sender.hasPermission(Permissions.COMMAND_SCOREBOARD_TOGGLE.getPermission())) {
                sender.sendMessage(Messages.NO_PERMISSION.toString());
                return;
            }

            if (!plugin.getModuleManager().isEnabled(ModuleType.SCOREBOARD)) {
                sender.sendMessage(TextUtil.color("&cThe scoreboard module is not enabled in the configuration."));
                return;
            }

            Player player = (Player) sender;
            ScoreboardManager scoreboardManager = ((ScoreboardManager) plugin.getModuleManager().getModule(ModuleType.SCOREBOARD));

            if (scoreboardManager.hasScore(player.getUniqueId())) {
                scoreboardManager.removeScoreboard(player);
                player.sendMessage(Messages.SCOREBOARD_TOGGLE.toString().replace("%value%", "disabled"));
            } else {
                scoreboardManager.createScoreboard(player);
                player.sendMessage(Messages.SCOREBOARD_TOGGLE.toString().replace("%value%", "enabled"));
            }
        }

		/*
		Command: info
		Description: displays useful information about the configuration
		*/
        else if (args.getString(0).equalsIgnoreCase("info")) {

            if (!sender.hasPermission(Permissions.COMMAND_DELUXEHUB_HELP.getPermission())) {
                sender.sendMessage(Messages.NO_PERMISSION.toString());
                return;
            }

            sender.sendMessage("");
            sender.sendMessage(TextUtil.color("&d&lPlugin Information"));
            sender.sendMessage("");

            Location location = ((LobbySpawn) plugin.getModuleManager().getModule(ModuleType.LOBBY)).getLocation();
            sender.sendMessage(TextUtil.color("&7Spawn set &8- " + (location != null ? "&ayes" : "&cno &7&o(/setspawn)")));

            sender.sendMessage("");

            ModuleManager moduleManager = plugin.getModuleManager();
            sender.sendMessage(TextUtil.color("&7Disabled Worlds (" + moduleManager.getDisabledWorlds().size() + ") &8- &a" + (String.join(", ", moduleManager.getDisabledWorlds()))));

            InventoryManager inventoryManager = plugin.getInventoryManager();
            sender.sendMessage(TextUtil.color("&7Custom menus (" + inventoryManager.getInventories().size() + ")" + " &8- &a" + (String.join(", ", inventoryManager.getInventories().keySet()))));

            HotbarManager hotbarManager = ((HotbarManager) plugin.getModuleManager().getModule(ModuleType.HOTBAR_ITEMS));
            sender.sendMessage(TextUtil.color("&7Hotbar items (" + hotbarManager.getHotbarItems().size() + ")" + " &8- &a" + (hotbarManager.getHotbarItems().stream().map(HotbarItem::getKey).collect(Collectors.joining(", ")))));

            CommandManager commandManager = plugin.getCommandManager();
            sender.sendMessage(TextUtil.color("&7Custom commands (" + commandManager.getCustomCommands().size() + ")" + " &8- &a" + (commandManager.getCustomCommands().stream().map(command -> command.getAliases().get(0)).collect(Collectors.joining(", ")))));

            sender.sendMessage("");

            sender.sendMessage(TextUtil.color("&7PlaceholderAPI Hook: " + (plugin.getHookManager().isHookEnabled("PLACEHOLDER_API") ? "&ayes" : "&cno")));
            sender.sendMessage(TextUtil.color("&7HeadDatabase Hook: " + (plugin.getHookManager().isHookEnabled("HEAD_DATABASE") ? "&ayes" : "&cno")));

            sender.sendMessage("");
        }

		/*
		Command: open
		Description: opens a custom menu
		*/
        else if (args.getString(0).equalsIgnoreCase("open")) {
            if (!(sender instanceof Player)) throw new CommandException("Console cannot open menus");

            if (!sender.hasPermission(Permissions.COMMAND_OPEN_MENUS.getPermission())) {
                sender.sendMessage(Messages.NO_PERMISSION.toString());
                return;
            }

            if (args.argsLength() == 1) {
                sender.sendMessage(TextUtil.color("&cUsage: /hubcore open <menu>"));
                return;
            }

            AbstractInventory inventory = plugin.getInventoryManager().getInventory(args.getString(1));
            if (inventory == null) {
                sender.sendMessage(TextUtil.color("&c" + args.getString(1) + " is not a valid menu ID."));
                return;
            }
            inventory.openInventory((Player) sender);

        }

    }
}
